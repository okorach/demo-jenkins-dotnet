projectKey = "demo:gitlab-jenkins-dotnet"
tags = "gitlab,jenkins,dotnet"
pipeline {
  agent any
  environment {
      SONAR_HOST_URL  = credentials('SONAR_HOST_URL')
      SONAR_TOKEN     = credentials('SONAR_TOKEN')
  }
  stages {
    stage('Code checkout') {
        steps {
          checkout([$class: 'GitSCM', branches: [[name: '**']], extensions: [[$class: 'CloneOption', noTags: false, reference: '', shallow: false]], userRemoteConfigs: [[credentialsId: 'GitLabPAT2', url: 'https://gitlab.com/okorach/demo-jenkins-dotnet']]])
        }
    }
    stage('SonarQube LATEST analysis - .Net') {
      steps {

        script {
          def dotnetScannerHome = tool 'Scanner for .Net Core'
          withSonarQubeEnv('SQ Latest') {
            updateGitlabCommitStatus name: 'dotnet-scanner', state: 'running'
            sh """
              cd comp-dotnet
              /usr/local/share/dotnet/dotnet ${dotnetScannerHome}/SonarScanner.MSBuild.dll begin /k:\"${projectKey}\" /n:\"GitLab / Jenkins / .Net Core\"
              /usr/local/share/dotnet/dotnet build
              /usr/local/share/dotnet/dotnet ${dotnetScannerHome}/SonarScanner.MSBuild.dll end
              curl -X POST -u $SONAR_TOKEN: \"$SONAR_HOST_URL/api/project_tags/set?project=${projectKey}&tags=${tags}\"
            """
            updateGitlabCommitStatus name: 'dotnet-scanner', state: 'success'
          }
        }
      }
    }
    stage("SonarQube Quality Gate") {
      steps {
        timeout(time: 5, unit: 'MINUTES') {
          script {
            def qg = waitForQualityGate()
            if (qg.status != 'OK') {
              updateGitlabCommitStatus name: 'quality-gate', state: 'failed'
              echo ".Net component quality gate failed: ${qg.status}, proceeding anyway"
            } else {
              updateGitlabCommitStatus name: 'quality-gate', state: 'success'
            }
            sh 'rm -f comp-dotnet/.sonarqube/out/.sonar/report-task.txt'
          }
        }
      }
    }
  }
}
